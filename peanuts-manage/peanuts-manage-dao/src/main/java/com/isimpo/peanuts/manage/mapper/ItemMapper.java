package com.isimpo.peanuts.manage.mapper;

import com.github.abel533.mapper.Mapper;
import com.isimpo.peanuts.manage.pojo.Item;

public interface ItemMapper extends Mapper<Item>{

}
