package com.isimpo.peanuts.manage.mapper;

import com.github.abel533.mapper.Mapper;
import com.isimpo.peanuts.manage.pojo.ItemDesc;

public interface ItemDescMapper extends Mapper<ItemDesc>{

}
