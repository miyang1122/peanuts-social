package com.isimpo.peanuts.manage.mapper;

import com.isimpo.peanuts.manage.pojo.ItemCat;
import com.github.abel533.mapper.Mapper;

public interface ItemCatMapper extends Mapper<ItemCat>{

}
