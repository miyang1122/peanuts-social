package com.isimpo.peanuts.manage.service;

import com.isimpo.peanuts.manage.pojo.Item;
import com.isimpo.peanuts.manage.vo.EasyUIResult;

public interface ItemService {

	void insertItem(Item item,String desc);

	void updateItem(Item item, String itemDesc);

	EasyUIResult getItem(Long page, Long rows,String orderByClause);
}
