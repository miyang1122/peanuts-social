package com.isimpo.peanuts.manage.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.abel533.entity.Example;
import com.github.abel533.entity.Example.Criteria;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.isimpo.peanuts.manage.mapper.ContentMapper;
import com.isimpo.peanuts.manage.pojo.Content;
import com.isimpo.peanuts.manage.service.ContentService;
import com.isimpo.peanuts.manage.vo.EasyUIResult;


@Service
public class ContentServiceImpl implements ContentService {

	@Autowired
	private ContentMapper contentMapper;
	
	
	
	public EasyUIResult queryContentByCatAndPage(Long categoryId, Integer page, Integer rows) {
		
		Content content=new Content();
		PageHelper.startPage(page,rows);
		content.setCategoryId(categoryId);
		List<Content> select = this.contentMapper.select(content);
		if(select!=null && select.size()>0){
			EasyUIResult result=new EasyUIResult();
			PageInfo pageInfo = new PageInfo(select);
			result.setTotal((int) pageInfo.getTotal());
			result.setRows(select);
			return result;
		}
		return null;
	}


	//插入内容
	public void insertContent(Content content) {
		content.setId(null);
		content.setCreated(new Date());
		content.setUpdated(new Date());
		this.contentMapper.insert(content);
	}


	
	public List<Content> queryByCats(List<Object> categoryIds) {
		
		
		
		Example example=new Example(Content.class);
		Criteria criteria = example.createCriteria();
		criteria.andIn("categoryId", categoryIds);
		List<Content> list = this.contentMapper.selectByExample(example);
		
		return list;
	}

}
