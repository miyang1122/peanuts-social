package com.isimpo.peanuts.manage.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.isimpo.peanuts.manage.mapper.ItemCatMapper;
import com.isimpo.peanuts.manage.pojo.ItemCat;
import com.isimpo.peanuts.manage.pojo.ItemCatQueryVo;
import com.isimpo.peanuts.manage.service.ItemCatService;
import com.isimpo.peanuts.manage.vo.ItemCatData;
import com.isimpo.peanuts.manage.vo.ItemCatResult;

@Service
public class ItemCatServiceImpl implements ItemCatService{

	@Autowired
	private ItemCatMapper itemCatMapper;
	
	@Autowired
	private RedisServiceImpl redisServiceImpl;
	
	@Value("${BIG_AD_KEY}")
	private String BIG_AD_KEY;
	
	public List<ItemCatQueryVo> queryItemCat(Long id) {
		
		ItemCat itemCat=new ItemCat();
		itemCat.setParentId(id);
		List<ItemCat> itemCats = itemCatMapper.select(itemCat);
		
		List<ItemCatQueryVo> list=new ArrayList<ItemCatQueryVo>();
		
		if(itemCats!=null && itemCats.size()>0){
			for (ItemCat cat : itemCats) {
				ItemCatQueryVo vo=new ItemCatQueryVo();
				vo.setId(cat.getId());
				vo.setText(cat.getName());
				vo.setState(cat.getIsParent()==true?"closed":"open");
				list.add(vo);
			}
		}
		
		return list;
	}

	public ItemCat queryItemCatById(Long id) {
		
		ItemCat itemCat = this.itemCatMapper.selectByPrimaryKey(id);
		return itemCat;
	}

	
	/**
	 * 查询所有商品
	 */
	public ItemCatResult queryCatAll() {
		
		try {
			ItemCatResult bean = this.redisServiceImpl.getBean(BIG_AD_KEY, new TypeReference<ItemCatResult>() {
			});
			if(bean!=null){
				return bean;
			}
		}  catch (Exception e) {
			e.printStackTrace();
		}
		
		// 查询出全部结果
        List<ItemCat> list = this.itemCatMapper.select(null);
        // 转为map存储，key为父节点ID，value为该父节点下所有子节点数据集合
        Map<Long, List<ItemCat>> itemCatMap = new HashMap<Long, List<ItemCat>>();
        for (ItemCat itemCat : list) {
            if (!itemCatMap.containsKey(itemCat.getParentId())) {
                itemCatMap.put(itemCat.getParentId(), new ArrayList<ItemCat>());
            }
            itemCatMap.get(itemCat.getParentId()).add(itemCat);
        }

        // 创建结果对象
        ItemCatResult result = new ItemCatResult();
        // 获取1级类目
        List<ItemCat> level1List = itemCatMap.get(0L);
        for (ItemCat itemCat : level1List) {
            // 创建1级数据对象
            ItemCatData data = new ItemCatData();
            // 将1级数据对象添加到1级类目集合
            result.getItemCats().add(data);
            data.setUrl("/products/" + itemCat.getId() + ".html");
            data.setName("<a href='" + data.getUrl() + "'>" + itemCat.getName() + "</a>");
            // 如果没有子节点，跳过
            if(!itemCat.getIsParent()){
                continue;
            }
            
            // 创建2级类目数据集合
            List<ItemCatData> level2DataList = new ArrayList<>();
            data.setItems(level2DataList);
            // 获取2级类目数据
            List<ItemCat> level2List = itemCatMap.get(itemCat.getId());
            for (ItemCat itemCat2 : level2List) {
                // 创建2级数据对象
                ItemCatData data2 = new ItemCatData();
                // 将2级数据对象添加到2级类目集合
                level2DataList.add(data2);
                data2.setUrl("/products/" + itemCat2.getId() + ".html");
                data2.setName(itemCat2.getName());
                // 如果没有子节点，跳过
                if(!itemCat2.getIsParent()){
                    continue;
                }

                // 创建3级类目数据集合
                List<String> leve13DataList = new ArrayList<>();
                data2.setItems(leve13DataList);
                // 查询3级类目
                List<ItemCat> level3List = itemCatMap.get(itemCat2.getId());
                for (ItemCat itemCat3 : level3List) {
                    // 封装3级类目数据到集合
                    leve13DataList.add("/products/" + itemCat3.getId() + ".html|" + itemCat3.getName());
                }
            }
			// 前台只需要前面14条1级类目的数据
            if (result.getItemCats().size() >= 14) {
                break;
            }
        }
        
        try {
			this.redisServiceImpl.set(BIG_AD_KEY, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return result;
	}
	

}
