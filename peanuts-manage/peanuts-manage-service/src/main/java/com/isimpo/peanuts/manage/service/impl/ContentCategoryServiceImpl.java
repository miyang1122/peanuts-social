package com.isimpo.peanuts.manage.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isimpo.peanuts.manage.mapper.ContentCategoryMapper;
import com.isimpo.peanuts.manage.pojo.ContentCategory;
import com.isimpo.peanuts.manage.service.ContentCategoryService;

@Service
public class ContentCategoryServiceImpl implements ContentCategoryService{

	@Autowired
	private ContentCategoryMapper contentCategoryMapper;
	
	/*@Autowired
	private ContentService contentService;*/
	
	public List<ContentCategory> queryByCondition(ContentCategory contentCategory) {
		
		return this.contentCategoryMapper.select(contentCategory);
	}

	
	
	public void insertContentCat(ContentCategory contentCategory) {
		
		contentCategory.setId(null);
		contentCategory.setCreated(new Date());
		contentCategory.setUpdated(new Date());
		contentCategory.setSortOrder(1);
		contentCategory.setIsParent(false);
		contentCategory.setStatus(1);
		this.contentCategoryMapper.insert(contentCategory);
		
		//更新父亲的isParent属性
		Long parentId = contentCategory.getParentId();
		ContentCategory category = this.contentCategoryMapper.selectByPrimaryKey(parentId);
		Boolean isParent = category.getIsParent();
		if(!isParent){
			ContentCategory cate=new ContentCategory();
			cate.setId(category.getId());
			cate.setIsParent(true);
			updateContentCatById(cate);
		}
	}


	public void updateContentCatById(ContentCategory contentCategory) {
		this.contentCategoryMapper.updateByPrimaryKeySelective(contentCategory);
		
	}



	public void deleteContentCatById(ContentCategory contentCategory) {
		
		Long id = contentCategory.getId();
		Long parentId = contentCategory.getParentId();
		
		//查看父节点有没有其它子节点
		ContentCategory c=new ContentCategory();
		c.setParentId(parentId);
		List<ContentCategory> select = this.contentCategoryMapper.select(c);
		if(select.size()<=1){
			ContentCategory cate=new ContentCategory();
			cate.setId(parentId);
			cate.setIsParent(false);
			updateContentCatById(cate);
		}
		
		//先删除
	    this.contentCategoryMapper.delete(contentCategory);
	    //查找所有的子节点，放入准备好的节点
	    List<ContentCategory> catList=new ArrayList<ContentCategory>();
	    findAllChild(id,catList);
	    if(catList!=null){
	    	for (ContentCategory c2 : catList) {
				this.contentCategoryMapper.deleteByPrimaryKey(c2.getId());
			}
	    	
	    }
	}

	//查找所有的子节点
	private void findAllChild(Long id, List<ContentCategory> catList) {
		ContentCategory c=new ContentCategory();
		c.setParentId(id);
		List<ContentCategory> list = this.contentCategoryMapper.select(c);
		if(list!=null&&list.size()>0){
			for (int i = 0; i <list.size(); i++) {
				catList.add(list.get(i));
				findAllChild(list.get(i).getId(), catList);
			}
		}
	}

}
