package com.isimpo.peanuts.manage.service;

import java.util.List;

import com.isimpo.peanuts.manage.pojo.ContentCategory;

public interface ContentCategoryService {

	List<ContentCategory> queryByCondition(ContentCategory contentCategory);

	void insertContentCat(ContentCategory contentCategory);
	
	void updateContentCatById(ContentCategory contentCategory);

	void deleteContentCatById(ContentCategory contentCategory);
	
	
}
