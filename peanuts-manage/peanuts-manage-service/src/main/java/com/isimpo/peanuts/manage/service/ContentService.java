package com.isimpo.peanuts.manage.service;

import java.util.List;

import com.isimpo.peanuts.manage.pojo.Content;
import com.isimpo.peanuts.manage.vo.EasyUIResult;

public interface ContentService {

	EasyUIResult queryContentByCatAndPage(Long categoryId, Integer page, Integer rows);

	void insertContent(Content content);

	List<Content> queryByCats(List<Object> categoryIds);

}
