package com.isimpo.peanuts.manage.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isimpo.peanuts.manage.mapper.ItemDescMapper;
import com.isimpo.peanuts.manage.pojo.ItemDesc;
import com.isimpo.peanuts.manage.service.ItemDescService;

@Service
public class ItemDescImpl implements ItemDescService{

	@Autowired
	private ItemDescMapper itemDescMapper;
	
	public void insertItemDesc(ItemDesc itemDesc) {
		
		itemDesc.setCreated(new Date());
		itemDesc.setUpdated(new Date());
		this.itemDescMapper.insert(itemDesc);
	}

	
	public ItemDesc getItemDescById(Long itemID) {
		
		ItemDesc itemDesc = itemDescMapper.selectByPrimaryKey(itemID);
		return itemDesc;
	}


	public void updateItemDesc(ItemDesc itemDesc) {
		this.itemDescMapper.updateByPrimaryKeySelective(itemDesc);
	}

}
