package com.isimpo.peanuts.manage.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.abel533.entity.Example;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.isimpo.peanuts.manage.mapper.ItemMapper;
import com.isimpo.peanuts.manage.pojo.Item;
import com.isimpo.peanuts.manage.pojo.ItemDesc;
import com.isimpo.peanuts.manage.service.ItemDescService;
import com.isimpo.peanuts.manage.service.ItemService;
import com.isimpo.peanuts.manage.vo.EasyUIResult;

@Service
public class ItemServiceImpl implements ItemService{

	@Autowired
	private ItemMapper itemMapper;
	
	@Autowired
	private ItemDescService itemDescService;
	
	public void insertItem(Item item,String desc) {
		item.setCreated(new Date());
		item.setUpdated(new Date());
		item.setId(null);
		//1 正常 2下架 3删除
		item.setStatus(1);
		itemMapper.insert(item);
		
		ItemDesc itemDesc=new ItemDesc();
		itemDesc.setItemId(item.getId());
		itemDesc.setItemDesc(desc);
		this.itemDescService.insertItemDesc(itemDesc);
		
	}

	
	public void updateItem(Item item, String desc) {
		item.setCreated(null);
		item.setUpdated(new Date());
		//item.setId(null);
		//1 正常 2下架 3删除
		item.setStatus(null);
		itemMapper.updateByPrimaryKeySelective(item);
		
		ItemDesc itemDesc=new ItemDesc();
		itemDesc.setItemId(item.getId());
		itemDesc.setItemDesc(desc);
		this.itemDescService.updateItemDesc(itemDesc);
		
		
	}

	
	
	public EasyUIResult getItem(Long page, Long rows,String orderByClause) {
		
		PageHelper.startPage(page.intValue(), rows.intValue());
		Example example=new Example(Item.class);
		example.setOrderByClause(orderByClause);
		List<Item> list = this.itemMapper.selectByExample(example);
		
		EasyUIResult result=new EasyUIResult();
		
		PageInfo pageinfo=new PageInfo(list);
		
		if(list!=null&&list.size()>0)
		result.setRows(list);
		result.setTotal((int) pageinfo.getTotal());
		
		return result;
	}

}
