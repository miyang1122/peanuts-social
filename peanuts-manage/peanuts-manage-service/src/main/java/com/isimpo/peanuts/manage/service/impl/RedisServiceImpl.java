package com.isimpo.peanuts.manage.service.impl;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.format.datetime.joda.ReadableInstantPrinter;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.isimpo.peanuts.manage.utils.JsonUtils;

@Service
public class RedisServiceImpl {
	
	@Autowired
	private StringRedisTemplate redisTemple;

	/**
	 * 新增
	 * @param key
	 * @param value
	 */
	public void set(String key,Object value){
		
		if(value.getClass()==String.class){
			redisTemple.opsForValue().set(key, (String) value);
		}
		redisTemple.opsForValue().set(key,JsonUtils.objectToJson(value));
	}
	
	/**
	 * 新增并设置过期时间
	 * @param key
	 * @param value
	 * @param time
	 */
    public void set(String key,Object value,long time){
		
		if(value.getClass()==String.class){
			redisTemple.opsForValue().set(key, (String) value,time,TimeUnit.SECONDS);
		}
		redisTemple.opsForValue().set(key,JsonUtils.objectToJson(value),time,TimeUnit.SECONDS);
	}
	
	/**
	 * 查询方法
	 * @param key
	 * @return
	 */
    public String get(String key){
    	return redisTemple.opsForValue().get(key);
    	
    }
    
    /**
     * 返回泛型
     * @param key
     * @param type
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    public <T> T getBean(String key,TypeReference<T> type) throws JsonParseException, JsonMappingException, IOException{
    	String json = this.redisTemple.opsForValue().get(key);
    	if(json!=null && !"".equals(json)){
    		return JsonUtils.toObject(json, type);
    	}
    	return null;
    }
	
    /**
     * 删除
     * @param key
     */
   public void del(String key){
	   this.redisTemple.delete(key);
   }
   
   /**
    * 设置过期时间
    * @param key
    * @param timeout
    */
   public void expire(String key,long timeout){
	   this.redisTemple.expire(key, timeout, TimeUnit.SECONDS);
   }
}
