package com.isimpo.peanuts.manage.service;

import com.isimpo.peanuts.manage.pojo.ItemDesc;

public interface ItemDescService {

	void insertItemDesc(ItemDesc itemDesc);

	ItemDesc getItemDescById(Long itemID);

	void updateItemDesc(ItemDesc itemDesc);
}
