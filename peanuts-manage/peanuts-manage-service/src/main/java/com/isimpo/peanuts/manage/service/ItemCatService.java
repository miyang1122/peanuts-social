package com.isimpo.peanuts.manage.service;

import java.util.List;

import com.isimpo.peanuts.manage.pojo.ItemCat;
import com.isimpo.peanuts.manage.pojo.ItemCatQueryVo;
import com.isimpo.peanuts.manage.vo.ItemCatResult;

public interface ItemCatService{
	
	public List<ItemCatQueryVo> queryItemCat(Long id);

	public ItemCat queryItemCatById(Long id);

	public ItemCatResult queryCatAll();
}

