package com.isimpo.peanuts.manage.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;


@ControllerAdvice(basePackages="com.isimpo.peanuts.manage.controller.api")
public class JsonpResponseAdvice extends AbstractJsonpResponseBodyAdvice{

	public JsonpResponseAdvice() {
		super("callback","jsonp");
	}

}
