package com.isimpo.peanuts.manage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.isimpo.peanuts.manage.pojo.Content;
import com.isimpo.peanuts.manage.service.ContentService;
import com.isimpo.peanuts.manage.vo.EasyUIResult;


@Controller
@RequestMapping("/content")
public class ContentController {

	@Autowired
	private ContentService contentService;
	
	/**
	 * 根据内容分类分页查询内容
	 * @param categoryId
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<EasyUIResult> queryContentByCatAndPage(@RequestParam("categoryId")Long categoryId,
			@RequestParam(value="page",defaultValue="1")Integer page, 
			@RequestParam(value="rows",defaultValue="10")Integer rows){
		
		try {
			EasyUIResult result=this.contentService.queryContentByCatAndPage(categoryId,page,rows);
			if(result==null){
			 return	ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			}
			return ResponseEntity.ok(result);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
		 
	}
	
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insertContent(Content content){
		
		try {
			//数据校验
			this.contentService.insertContent(content);
			
			return ResponseEntity.status(HttpStatus.CREATED).build();
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		
	}
}
