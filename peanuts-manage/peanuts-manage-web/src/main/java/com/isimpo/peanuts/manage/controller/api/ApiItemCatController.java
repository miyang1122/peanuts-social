package com.isimpo.peanuts.manage.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isimpo.peanuts.manage.service.ItemCatService;
import com.isimpo.peanuts.manage.utils.JsonUtils;
import com.isimpo.peanuts.manage.vo.ItemCatResult;

@Controller
public class ApiItemCatController {

	@Autowired
	private ItemCatService itemCatService;
	
	/*@RequestMapping(value="/api/item/cat",produces=MediaType.APPLICATION_JSON_VALUE+";charset=utf-8")
	@ResponseBody*/
	public String queryItemCatAll(@RequestParam("callback") String callback){
		
		try {
			ItemCatResult itemCatResult=this.itemCatService.queryCatAll();
			
			if(callback!=null&&!"".equals(callback)){
				String finalResult=callback+"("+JsonUtils.objectToJson(itemCatResult)+")";
				return finalResult;
			}
			return JsonUtils.objectToJson(itemCatResult);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	@RequestMapping(value="/api/item/cat")
	public ResponseEntity<ItemCatResult> queryItemCatAll2(){
		
		try {
			ItemCatResult itemCatResult=this.itemCatService.queryCatAll();
			if(itemCatResult==null)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			
			return ResponseEntity.ok(itemCatResult);
		} catch (Exception e) {
			e.printStackTrace();
		  return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}

}
