package com.isimpo.peanuts.manage.controller;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonAnyFormatVisitor;
import com.isimpo.peanuts.manage.pojo.ItemCat;
import com.isimpo.peanuts.manage.pojo.ItemCatQueryVo;
import com.isimpo.peanuts.manage.service.ItemCatService;
import com.isimpo.peanuts.manage.utils.JsonUtils;
import com.isimpo.peanuts.manage.vo.ItemCatResult;

@Controller
@RequestMapping("item/cat")
public class ItemCatController {
	
	
	@Autowired
	private ItemCatService itemCatService;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ItemCatQueryVo>> 
	queryItemCat(@RequestParam(value="id",defaultValue="0")Long parentId){
		
		try {
			List<ItemCatQueryVo> list=this.itemCatService.queryItemCat(parentId);
			
			if(CollectionUtils.isEmpty(list)){
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			}
			return ResponseEntity.ok(list);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}
	
	/**
	 * 通过id查询商品分类
	 * @param id
	 * @return
	 */
	@RequestMapping(value="catagory/{id}",method=RequestMethod.GET)
	public ResponseEntity<ItemCat> queryItemCatById(@PathVariable("id")Long id){
		
		try {
			ItemCat item=this.itemCatService.queryItemCatById(id);
			
			if(item==null){
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			}
			return ResponseEntity.ok(item);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}
	
	
	
	
	@RequestMapping(value="api/item/cat",produces=MediaType.APPLICATION_JSON_VALUE+";charset=utf-8")
	@ResponseBody
	public String queryItemCatAll(@RequestParam("callback") String callback){
		
		try {
			ItemCatResult itemCatResult=this.itemCatService.queryCatAll();
			
			if(callback!=null&&!"".equals(callback)){
				String finalResult=callback+"("+JsonUtils.objectToJson(itemCatResult)+")";
				return finalResult;
			}
			return JsonUtils.objectToJson(itemCatResult);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
