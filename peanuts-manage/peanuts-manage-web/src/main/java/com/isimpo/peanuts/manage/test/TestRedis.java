package com.isimpo.peanuts.manage.test;

import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class TestRedis {

	
	public void testRedisClient(){
		
		JedisPoolConfig config=new JedisPoolConfig();
		config.setMaxTotal(50);
		config.setMaxWaitMillis(1000);
		
		JedisPool pool=new JedisPool(config, "192.168.25.130",6379);
		Jedis jedis = pool.getResource();
		jedis.set("miyang","go home");
		jedis.set("hello", "world");
		String str = jedis.get("hello");
		jedis.close();
		pool.close();
		System.out.println(str);
	}
	
	public void testRedisTemplete(){
      RedisTemplate<String,String> redisTemplate;
		
		JedisPoolConfig config=new JedisPoolConfig();
		config.setMaxTotal(50);
		config.setMaxWaitMillis(1000);
		config.setTestOnBorrow(true);
		JedisConnectionFactory connectionFactory =new JedisConnectionFactory(config);
		connectionFactory.setHostName("192.168.25.130");
		connectionFactory.setPort(6379);
		connectionFactory.setUsePool(true);
		
		connectionFactory.afterPropertiesSet();
		redisTemplate = new RedisTemplate<String,String>();
		redisTemplate.setConnectionFactory(connectionFactory);
		redisTemplate.afterPropertiesSet();
		
		
		ValueOperations<String, String> value = redisTemplate.opsForValue();
		value.set("hi", "byebye123");
		System.out.println(value.get("hi"));
	}
	
	/*public static void main(String[] args) {
		
		RedisTemplate<String, String> redisTemplate;
		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        // 设置最大连接数
        jedisPoolConfig.setMaxTotal(50);
        // 设置获取连接的最大等待时间
        jedisPoolConfig.setMaxWaitMillis(1000);
        // 开启获取连接时可用性校验，保证拿到的连接都是可用的
        jedisPoolConfig.setTestOnBorrow(true);

        // 构建Spring-data-redis连接工厂，其实就是连接池
        JedisConnectionFactory connectionFactory = new JedisConnectionFactory(jedisPoolConfig);
        // 设置IP
        connectionFactory.setHostName("192.168.25.130");
        // 设置端口
        connectionFactory.setPort(6379);
        // 设置是否使用连接池
        connectionFactory.setUsePool(true);
        // 执行初始化
        connectionFactory.afterPropertiesSet();

        // 创建RedisTemplate
        redisTemplate = new RedisTemplate<String, String>();
        // 连接工厂
        redisTemplate.setConnectionFactory(connectionFactory);
		
        redisTemplate.afterPropertiesSet();
        
        ValueOperations<String, String> value = redisTemplate.opsForValue();
		value.set("hi", "byebye");
		System.out.println(value.get("hi"));
	}*/
}
