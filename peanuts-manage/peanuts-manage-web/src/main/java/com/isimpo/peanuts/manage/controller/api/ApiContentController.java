package com.isimpo.peanuts.manage.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.isimpo.peanuts.manage.pojo.Content;
import com.isimpo.peanuts.manage.service.ContentService;

@Controller
@RequestMapping("api/content")
public class ApiContentController {

	@Autowired
	private ContentService contentService;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Content>> 
	queryContent(@RequestParam("categoryIds") List<Object> categoryIds){
		
		try {
			List<Content> list=this.contentService.queryByCats(categoryIds);
			if(list==null || list.size()==0){
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			}
			return ResponseEntity.ok(list);
		} catch (Exception e) {
			e.printStackTrace();
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
		
	}
	
}
