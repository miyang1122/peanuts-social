package com.isimpo.peanuts.manage.controller;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.isimpo.peanuts.manage.pojo.ItemCatQueryVo;

@Controller
@RequestMapping("page")
public class ManagerController {

	
	
	/**
	 * 跳转到指定页面
	 * @param pageName
	 * @return
	 */
	@RequestMapping(value="{pageName}",method=RequestMethod.GET)
	public String toPage(@PathVariable("pageName")String pageName){
		
		return pageName;
	}
	
	
	
	
}
