package com.isimpo.peanuts.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.isimpo.peanuts.manage.pojo.ContentCategory;
import com.isimpo.peanuts.manage.service.ContentCategoryService;

@Controller
@RequestMapping("/content/category")
public class ContentCatController {

	@Autowired
	private ContentCategoryService contentCategoryService;
	
	/**
	 * 查询内容分类
	 */
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ContentCategory>> quueryContentCat(@RequestParam(value="id",defaultValue="0")Long id){
		
		try {
			ContentCategory contentCategory=new ContentCategory();
			contentCategory.setParentId(id);
			List<ContentCategory> list=this.contentCategoryService.queryByCondition(contentCategory);
			if(list==null){
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			}
			return ResponseEntity.ok(list);
		} catch (Exception e) {
			e.printStackTrace();
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
		
	}
	
	/**
	 * 新增内容分类
	 * @param contentCategory
	 * @return
	 */
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<ContentCategory> insertContentCat(ContentCategory contentCategory){
		try {
			
			this.contentCategoryService.insertContentCat(contentCategory);
			return ResponseEntity.status(HttpStatus.CREATED).body(contentCategory);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
		
	}
	
	
	/**
	 * 修改内容分类
	 * @param id
	 * @param name
	 * @return
	 */
	@RequestMapping(method=RequestMethod.PUT)
	public ResponseEntity<Void> updateContentCat(@RequestParam("id")Long id,@RequestParam("name")String name){
		try {
			ContentCategory contentCategory=new ContentCategory();
			contentCategory.setId(id);
			contentCategory.setName(name);
			this.contentCategoryService.updateContentCatById(contentCategory);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}
	
	
	@RequestMapping(method=RequestMethod.DELETE)
	public ResponseEntity<Void> deleteContentCat(@RequestParam("id")Long id,@RequestParam("parentId")Long parentId){
		try {
			ContentCategory contentCategory=new ContentCategory();
			contentCategory.setId(id);
			contentCategory.setParentId(parentId);
			this.contentCategoryService.deleteContentCatById(contentCategory);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}
}
