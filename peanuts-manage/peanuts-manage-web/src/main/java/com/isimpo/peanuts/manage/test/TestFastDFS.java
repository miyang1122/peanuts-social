package com.isimpo.peanuts.manage.test;

import java.io.File;
import java.nio.file.Files;

import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient1;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;

public class TestFastDFS {

	//@Test
	public void testFastDfs() throws Exception{
		
		File file=new File("F:/360downloads/city1.jpg");
		ClientGlobal.init("F:/workspace/eclipsedemo1/peanuts-manage/peanuts-manage-web/src/main/resources/fdfs_client.conf");
		TrackerClient trackerClient =new TrackerClient();
		
		TrackerServer server=trackerClient.getConnection();
		
		StorageClient1 storageClient1=new StorageClient1(server,null);
		
		String extension = file.getName().substring(file.getName().lastIndexOf(".")+1);
		
		String upload_path = storageClient1.upload_file1(Files.readAllBytes(file.toPath()), extension, null);
		
		System.out.println(upload_path);
		
	}
	
}
