package com.isimpo.peanuts.manage.test;

import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import redis.clients.jedis.JedisPoolConfig;

public class RedisTemplateDemo {

    private static RedisTemplate<String, String> redisTemplate;

    static {
        // 构建连接池配置信息
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        // 设置最大连接数
        jedisPoolConfig.setMaxTotal(50);
        // 设置获取连接的最大等待时间
        jedisPoolConfig.setMaxWaitMillis(1000);
        // 开启获取连接时可用性校验，保证拿到的连接都是可用的
        jedisPoolConfig.setTestOnBorrow(true);

        // 构建Spring-data-redis连接工厂，其实就是连接池
        JedisConnectionFactory connectionFactory = new JedisConnectionFactory(jedisPoolConfig);
        // 设置IP
        connectionFactory.setHostName("192.168.25.130");
        // 设置端口
        connectionFactory.setPort(6379);
        // 设置是否使用连接池
        connectionFactory.setUsePool(true);
        // 执行初始化
        connectionFactory.afterPropertiesSet();

        // 创建RedisTemplate
        redisTemplate = new RedisTemplate<String, String>();
        // 连接工厂
        redisTemplate.setConnectionFactory(connectionFactory);
        // 设置key的序列化方式为String类型
        // StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        // redisTemplate.setKeySerializer(stringRedisSerializer);
        // redisTemplate.setHashKeySerializer(stringRedisSerializer);
        // 设置值的序列化方式
        // Jackson2JsonRedisSerializer<Object> jsonRedisSerializer = new
        // Jackson2JsonRedisSerializer<Object>(Object.class);
        // redisTemplate.setValueSerializer(jsonRedisSerializer);
        // redisTemplate.setHashValueSerializer(jsonRedisSerializer);

        // 初始化
        redisTemplate.afterPropertiesSet();
    }

    public static void main(String[] args) {
        // 测试字符串结构操作
        testOpsForValue();
        // 测试hash结构操作
       // testOpsForHash();
    }

    public static void testOpsForValue() {
        // 添加数据
        redisTemplate.opsForValue().set("num", "123");
        // 获取数据
        System.out.println(redisTemplate.opsForValue().get("num"));
    }

    public static void testOpsForHash() {
        // 添加数据
        redisTemplate.opsForHash().put("user", "name", "Jack");
        redisTemplate.opsForHash().put("user", "age", 20);

        // 获取数据,hset操作
        System.out.println(redisTemplate.opsForHash().get("user", "name"));

        // 获取所有key, keys操作
        System.out.println(redisTemplate.opsForHash().keys("user"));

        // 获取所有值，values操作
        System.out.println(redisTemplate.opsForHash().values("user"));
    }
}
