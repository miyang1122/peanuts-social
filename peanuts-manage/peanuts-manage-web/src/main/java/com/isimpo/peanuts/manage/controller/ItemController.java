package com.isimpo.peanuts.manage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.isimpo.peanuts.manage.pojo.Item;
import com.isimpo.peanuts.manage.service.ItemDescService;
import com.isimpo.peanuts.manage.service.ItemService;
import com.isimpo.peanuts.manage.vo.EasyUIResult;

@RequestMapping("item")
@Controller
public class ItemController {

	@Autowired
	private ItemService itemService;
	
	@Autowired
	private ItemDescService itemDescService;
	
	/**
	 * 新增商品
	 * @param item
	 * @param itemDesc
	 * @return
	 */
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> saveItem(Item item,@RequestParam("desc")String itemDesc){
		
		try {
			//数据校验
			
			if(item!=null)
			this.itemService.insertItem(item,itemDesc);
			
			return ResponseEntity.status(HttpStatus.CREATED).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		
	}
	
	
	@RequestMapping(method=RequestMethod.PUT)
	public ResponseEntity<Void> updateItem(Item item,@RequestParam("desc")String itemDesc){
		
		try {
			//数据校验
			
			if(item!=null)
				this.itemService.updateItem(item,itemDesc);
			
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		
	}
	
	
	/**
	 * 查询商品
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<EasyUIResult> getItem(@RequestParam(value="page",defaultValue="1")Long page,@RequestParam(value="rows",defaultValue="10")Long rows){
		
		try {
			//数据校验
			
			
			EasyUIResult easyUIResult=this.itemService.getItem(page,rows,"updated DESC");
			if(easyUIResult==null){
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			}
			
			return ResponseEntity.ok(easyUIResult);
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
		
	}
	
	
	
	
	
	
	
	
}
