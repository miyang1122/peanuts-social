package com.isimpo.peanuts.manage.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient1;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.isimpo.peanuts.manage.utils.FastDFSClient;
import com.isimpo.peanuts.manage.vo.PicUploadResult;

@RequestMapping("pic")
@Controller
public class FileuploadController {

	private final static List<String> EXTENSIONS=Arrays.asList(".jpg",".png");
	
	@RequestMapping("upload")
	@ResponseBody
	public PicUploadResult upload(@RequestParam("uploadFile")MultipartFile uploadFile,
			HttpServletRequest request){
		
		PicUploadResult result=new PicUploadResult();
		result.setError(1);
		
		if(uploadFile==null){
			return result;
		}
		
		try {
			String fileName=uploadFile.getOriginalFilename();
			//验证文件的有效性
			String substring = fileName.substring(fileName.lastIndexOf("."));
			
			if(!EXTENSIONS.contains(substring)){
				return result;
			}
			
			BufferedImage read = ImageIO.read(uploadFile.getInputStream());
			if(read==null){
				return result;
			}
			
			/*String realPath = request.getSession().getServletContext().getRealPath("upload");
			uploadFile.transferTo(new File(realPath,fileName));
			result.setError(0);
			result.setUrl("http://localhost:8081/upload/"+fileName);*/
			
			//使用fastDFS存储图片
			FastDFSClient client=new FastDFSClient("fdfs_client.conf");
			String upload_path = client.uploadFile(uploadFile.getBytes(), fileName);
			
			result.setError(0);
			result.setUrl("http://image.huasheng.com/"+upload_path);
			
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
